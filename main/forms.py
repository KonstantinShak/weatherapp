import datetime

from django import forms
from django.core.exceptions import ValidationError

from .models import Weather, WeatherType, Towns
from .utils.parsers import parse_data


class MultipleForm(forms.Form):
    action = forms.CharField(max_length=60, widget=forms.HiddenInput())


class BaseLineForm(MultipleForm):
    date = forms.DateTimeField(widget=forms.DateTimeInput(attrs={
        "class": "form-control",
        "type": "date",
        }), label="Date", required=False)
    time = forms.IntegerField(widget=forms.NumberInput(attrs={
        "class": "form-control",
        "placeholder": "Time",
        }), label="Time", required=False)
    town = forms.CharField(widget=forms.TextInput(attrs={
        "class": "form-control",
        "placeholder": "Town"
        }), label="Town", required=False)
    temp = forms.IntegerField(widget=forms.NumberInput(attrs={
        "class": "form-control",
        "placeholder": "Temperature"
        }), label="Temperature", required=False)
    weath = forms.CharField(widget=forms.TextInput(attrs={
        "class": "form-control",
        "placeholder": "Weather Type"
        }), label="Weather type", required=False)

    def clean_temp(self):
        if self.cleaned_data['temp']:
            temp = self.cleaned_data['temp']
            if int(temp) < -50 or int(temp) > 70:
                raise ValidationError('Температура должна быть от -50 до 70')
        return self.cleaned_data['temp']

    def clean_weath(self):
        if self.cleaned_data['temp'] and self.cleaned_data['weath'] != '':
            weath = self.cleaned_data['weath']
            temp = self.cleaned_data['temp']
            if weath == 'Снег' and int(temp) >= 0:
                raise ValidationError('Измените погоду или температуру')
            if weath == 'Дождь' and int(temp) < 0:
                raise ValidationError('Измените погоду или температуру')
        return self.cleaned_data['weath']
    
    def clean_town(self):
        if self.cleaned_data['town']:
            if not self.cleaned_data['town'].isalpha():
                raise ValidationError('Уберите цифры')
        return self.cleaned_data['town']

class FilterForm(BaseLineForm):
    
    def clean_town(self):
        if self.cleaned_data['town']:
            if not self.cleaned_data['town'].isalpha():
                raise ValidationError('Уберите цифры')
        return self.cleaned_data['town']



class AddForm(BaseLineForm):

    def clean(self):
        values = self.cleaned_data.values()
        if None in values or '' in values:
            raise ValidationError('Заполните все поля')
        return self.cleaned_data

    def clean_town(self):
        super(AddForm, self).clean_town()
        if self.cleaned_data['date'] != None and self.cleaned_data['town'] != '':
            if Towns.objects.filter(town=self.cleaned_data['town']).exists():
                town = Towns.objects.get(town=self.cleaned_data['town'])
                time = self.cleaned_data['time']
                date = self.cleaned_data['date'] + datetime.timedelta(hours=time, minutes=00)
                if Weather.objects.filter(town=town, date=date).exists():
                    raise ValidationError('Такая запись уже существует')
        return self.cleaned_data['town']

    def save(self, some_dict):
        new_dict = parse_data(some_dict)
        p = Weather(
                date=new_dict['date'],
                town=Towns.objects.get_or_create(town=new_dict['town'])[0],
                temp=new_dict['temp'],
                weath=WeatherType.objects.get_or_create(w_type=new_dict['weath'])[0]
                )
        p.save()


class EditForm(BaseLineForm):
    
    def edit(self, some_dict, linepk):
        print('ura!')
        some_dict.pop('action')

        if some_dict['date'] and some_dict['time']:
            time = some_dict.pop('time')
            some_dict['date'] += datetime.timedelta(hours=time, minutes=00)
        else:
            if some_dict['date']:
                line = line = Weather.objects.get(id=linepk)
                some_dict['date'] = datetime.datetime.combine(some_dict['date'], line.date.time())
                some_dict.pop('time')
            elif some_dict['time']:
                time = some_dict.pop('time')
                line = Weather.objects.get(id=linepk)
                date = line.date
                some_dict['date'] = date.replace(hour=time, minute=00)
            else:
                some_dict.pop('time')

        if some_dict['town'] == '':
            some_dict['town'] = None
        else:
            some_dict['town'] = Towns.objects.get_or_create(town=some_dict['town'])[0]

        if some_dict['weath'] == '':
            some_dict['weath'] = None
        else:
            some_dict['weath'] = WeatherType.objects.get_or_create(w_type=some_dict['weath'])[0]

        new_dict = some_dict.copy()
        
        for item, value in some_dict.items():
            if not value:
                del new_dict[item]
    
        Weather.objects.filter(pk=linepk).update(**new_dict)
