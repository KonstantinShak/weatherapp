import django_filters
from django_filters import DateFilter

from .models import Weather


class WeatherFilter(django_filters.FilterSet):

    start_date = DateFilter(field_name="date", lookup_expr='gte', label='Date start with:')
    end_date = DateFilter(field_name="date", lookup_expr='lte', label='Date end with:')

    class Meta:
        model = Weather
        fields = '__all__'
        exclude = ['date', 'temp']
        