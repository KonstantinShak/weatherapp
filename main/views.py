from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.shortcuts import render

from .multiforms import MultiFormsView
from .forms import FilterForm, AddForm, EditForm
from .models import Weather, Towns


def form_redir(request):
    return render(request, 'index.html')


def delete(request, id):
    some_line = Weather.objects.get(id=id)
    some_line.delete()
    return HttpResponseRedirect(reverse('home'))


class Home(MultiFormsView):

    form_classes = {
            "filter": FilterForm,
            "add": AddForm,
            "edit": EditForm,
            }

    template_name = 'main/index.html'

    success_urls = {
            "filter": reverse_lazy('home'),
            "add": reverse_lazy('home'),
            "edit": reverse_lazy('home')
            }


    def filter_form_valid(self, form, linepk):
        form_name = form.cleaned_data.get('action')
        print(form.cleaned_data)
        return HttpResponseRedirect(self.get_success_url(form_name))

    def add_form_valid(self, form, linepk):
        form_name = form.cleaned_data.get('action')
        form.save(form.cleaned_data)
        return HttpResponseRedirect(self.get_success_url(form_name))

    def edit_form_valid(self, form, linepk):
        form_name = form.cleaned_data.get('action')
        form.edit(form.cleaned_data, linepk)
        return HttpResponseRedirect(self.get_success_url(form_name))

    def get_dashboard(self):
        data = list(Weather.objects.all().values())
        dateline = []
        townlist = []

        for item in data:
            if item['date'] not in dateline:
                dateline.append(item['date'])
            
            if item['town_id'] not in townlist:
                townlist.append(item['town_id'])

        dateline = sorted(dateline)

        row_towndict = {}
        towndict = {}

        for town in townlist:
            row_towndict[town] = []
            for date in dateline:
                flag = None
                for item in data:
                    if item['town_id'] == town and item['date'] == date:
                        flag = item['temp']

                if flag:
                    row_towndict[town].append(flag)
                else:
                    row_towndict[town].append(None)
        
        for item, value in row_towndict.items():
            towndict[str(Towns.objects.get(id=item))] = value
        
        return towndict, dateline


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['towndict'], context['timeline']  = self.get_dashboard()
        return context
    

