from django import template

from main.models import Weather


register = template.Library()


@register.inclusion_tag("main/table.html")
def get_table():
    some_strings = Weather.objects.all()
    return {"table": some_strings}


@register.simple_tag(name='get_table_filter')
def get_table_edit():
    return Weather.objects.all()
