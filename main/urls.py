from django.urls import path
from .views import *
from . import views

urlpatterns = [
        path('', Home.as_view(), name="home"),
        path('delete/<int:id>', views.delete, name='delete'),
]
