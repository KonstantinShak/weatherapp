from django.db import models


class Towns(models.Model):
    town = models.CharField(unique=True, max_length=32)

    def __str__(self):
        return self.town

class WeatherType(models.Model):
    w_type = models.CharField(unique=True, max_length=32)

    def __str__(self):
        return self.w_type

class Weather(models.Model):
    date = models.DateTimeField()
    town = models.ForeignKey('Towns', on_delete=models.PROTECT)
    temp = models.IntegerField()
    weath = models.ForeignKey('WeatherType', on_delete=models.PROTECT)

    def __str__(self):
        return str(self.town) + str(self.date)
