import json

from ..models import Weather, WeatherType, Towns
from .parsers import parse_data_json



class UploadingWeather(object):

    def __init__(self, data) -> None:
        data = data
        self.uploaded_file = data.get("file")
        self.parse()

    def parse(self) -> None:
        uploaded_file = self.uploaded_file
        self.uploaded_data = json.loads(uploaded_file.read())
        
    def save(self) -> None:
        uploaded_data = self.uploaded_data
        print(uploaded_data)
        if self.data_valid(uploaded_data):
            for item in uploaded_data["items"]:
                redy_data = parse_data_json(item)
                if self.value_valid(redy_data):
                    p = Weather(
                            date=redy_data['date'],
                            town=Towns.objects.get_or_create(town=redy_data['town'])[0],
                            temp=redy_data['temp'],
                            weath=WeatherType.objects.get_or_create(w_type=redy_data['weath'])[0]
                            )
                    p.save()
    
    def data_valid(self, data: dict) -> bool:
        if self.filds_valid(data['items']):
            return True
        return False
    
    def filds_valid(self, data: list) -> bool:
        items = data
        for item in items:
            if len(item) == 5:
                    return True
        return False

    def value_valid(self, data) -> bool:
        date = data['date']
        town = data['town']
        temp = data['temp']
        weath = data['weath']
        if weath == 'Снег' and int(temp) >= 0:
            return False
        if weath == 'Дождь' and int(temp) < 0:
            return False
        if not town.isalpha():
            return False
        if int(temp) < -50 or int(temp) > 70:
            return False
        town_id = Towns.objects.get_or_create(town=town)[0]
        if Weather.objects.filter(town=town_id, date=date).exists():
            return False
        return True