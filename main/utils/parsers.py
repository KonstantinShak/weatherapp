from typing import TypedDict
import datetime


class WeatherString(TypedDict):
    date: datetime.datetime
    town: str
    temp: int
    weath: str


def parse_data(some_dict: dict) -> WeatherString:
    '''pase data from POST'''
    some_dict.pop('action')
    time = some_dict.pop('time')
    some_dict['date'] += datetime.timedelta(hours=time, minutes=00)
    return WeatherString(**some_dict)


def parse_data_json(some_dict: dict) -> WeatherString:
    '''parse data from json'''
    format = "%Y-%m-%d"
    some_dict['date'] = datetime.datetime.strptime(some_dict['date'], format)
    time = some_dict.pop('time')
    some_dict['date'] += datetime.timedelta(hours=time, minutes=00)
    return WeatherString(**some_dict)
