from django.contrib import admin

from .models import Towns, Weather, WeatherType


admin.site.register(Towns)
admin.site.register(Weather)
admin.site.register(WeatherType)
